const mongoose = require('mongoose');

function MongoClient() { }
MongoClient.prototype.initialize = function () {
    mongoose
        .connect(process.env.DB_URL)
        .then(() => console.log('Successfully connected to database'))
        .catch(error => {
            throw error;
        });
};

MongoClient.prototype.mongify = function (id) {
    return mongoose.Types.ObjectId(id);
};

module.exports = new MongoClient();
