const router = require('express').Router();

const controller = require('./lib/controller');
const middleware = require('./lib/middleware');

router.get('/view', middleware.verifyToken, controller.viewProduct);

module.exports = router;
