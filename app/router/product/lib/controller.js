const { Product, Category, Cart } = require('../../../model');

const controller = {}

controller.viewProduct = async (req, res) => {
    try {
        const query = [{
            $lookup: {
                from: "categories",
                localField: "iCategory",
                foreignField: '_id',
                as: 'sCategory',
            }
        },
        {
            $unwind: '$sCategory',
        },
        {
            $addFields: {
                _id: { $toString: '$_id' },
                sCategoryName: '$sCategory.sName',
            },
        },
        {
            $project: {
                sTitle: 1,
                nPrice: 1,
                sDescription: 1,
                sCategoryName: 1,
                eStatus: 1,
                createdAt: 1,
            },
        }]

        const products = await Product.aggregate(query);
        return res.reply(message.success(), products);
    } catch (error) {
        console.log(error);
        return res.reply(message.server_error(), error.toString());
    }
};

module.exports = controller;