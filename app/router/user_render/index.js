const router = require('express').Router();

const controller = require('./lib/controller');
const middleware = require('./lib/middleware');

router.get('/login', controller.login);
router.get('/register', controller.register);
router.get('/', middleware.checkAuth, controller.landing);
router.get('/cart', middleware.checkAuth, controller.cart);

module.exports = router;