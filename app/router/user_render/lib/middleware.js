const { User } = require('../../../model');

const middleware = {};

middleware.checkAuth = (req, res, next) => {
    if (req.session['user_id'] == null && req.session['user_id'] == undefined) {
        res.redirect('login');
    } else {
        return next();
    }
}

module.exports = middleware;