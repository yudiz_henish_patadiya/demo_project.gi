const controller = {};

controller.landing = (req, res) => {
    return res.render('index');
};

controller.register = (req, res) => {
    return res.render('page-register');
};

controller.login = (req, res) => {
    return res.render('page-login');
};

controller.cart = (req, res) => {
    return res.render('page-cart');
};

module.exports = controller;