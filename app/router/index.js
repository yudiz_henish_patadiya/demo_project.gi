const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const express = require('express');

const routes = require('./router');
const fileRender = require('./render');

function Router() {
    this.app = express();
    this.corsOptions = {
        'Access-Control-Allow-Origin': '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE'],
        allowedHeaders: ['Content-Type', 'Authorization'],
        exposedHeaders: ['Authorization'],
    };
}

Router.prototype.initialize = function () {
    this.setupMiddleware();
    this.setupServer();
}

Router.prototype.setupMiddleware = function () {
    this.app.use(cors(this.corsOptions));
    this.app.use(session({
        secret: process.env.TOKEN_KEY,
        resave: true,
        saveUninitialized: true
    }));
    this.app.use(express.static('./seeds'));
    this.app.set('views', './seeds');
    this.app.set('view engine', 'ejs');
    this.app.use(bodyParser.json({ limit: '16mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '16mb', extended: true }));
    // this.app.use(cookie());
    this.app.use(this.routeConfig);

    this.app.use('/api/v1', routes);
    this.app.use('/', fileRender);
    this.app.use('*', this.routeHandler);
}

Router.prototype.setupServer = function () {
    this.app.listen(process.env.PORT, () => console.log(`app listening on port ${process.env.PORT}!`));
}

Router.prototype.routeConfig = function (req, res, next) {
    res.reply = ({ code, message }, data = {}, header = undefined) => {
        res.status(code)
            .header(header)
            .json({ message, data });
    };
    next();
};

Router.prototype.routeHandler = function (req, res) {
    return res.render('assets/error/page-error-404');
}

module.exports = new Router();
