const router = require('express').Router();

const controller = require('./lib/controller');
const middleware = require('./lib/middleware');

router.post('/register', controller.register);
router.post('/login', controller.login);
router.post('/logout', middleware.verifyToken, controller.logout);

module.exports = router;
