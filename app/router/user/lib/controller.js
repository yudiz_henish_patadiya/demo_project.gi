const { User } = require('../../../model');

const controller = {}

controller.register = async (req, res) => {
    try {
        const body = _.pick(req.body, ['sEmail', 'sPassword', 'sFirstname', 'sLastname', 'nContactNo']);
        if (!body.sEmail) return res.reply(message.required_field('Email'));
        if (!body.sPassword) return res.reply(message.required_field('Password'));
        if (!body.sFirstname) return res.reply(message.required_field('Firstname'));

        const query = {
            sEmail: body.sEmail,
        };
        const user = await User.findOne(query);
        if (user) return res.reply(message.already_exists('User'));

        body.sPassword = _.encryptPassword(body.sPassword);
        await User.create(body);
        return res.reply(message.successfully(`user created`));
    } catch (error) {
        console.log(error);
        return res.reply(message.server_error(), error.toString());
    }
};

controller.login = async (req, res) => {
    try {
        const body = _.pick(req.body, ['sEmail', 'sPassword']);
        if (!body.sEmail) return res.reply(message.required_field('Email'));
        if (!body.sPassword) return res.reply(message.required_field('Password'));

        const user = await User.findOne({ sEmail: body.sEmail });
        if (!user) return res.reply(message.not_found('User'));
        if (user.eStatus === 'n') return res.reply(message.blocked('Account'));
        if (user.eStatus === 'd') return res.reply(message.deleted('Account'));
        if (user.sPassword !== _.encryptPassword(body.sPassword)) return res.reply(message.wrong_credentials());

        user.sToken = _.encodeToken({ _id: user._id });
        await user.save();
        req.session['user_id'] = user._id;
        return res.reply(message.successfully('Logged in'), { sFirstname: user.sFirstname, sToken: user.sToken }, { Authorization: user.sToken });
    } catch (error) {
        return res.reply(message.server_error(), error.toString());
    }
};

controller.logout = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.userProfile._id, { $unset: { sToken: 1 } });
        if (!user) return res.reply(message.not_found('user'));
        req.session.destroy();
        return res.reply(message.successfully('Logout'));
    } catch (err) {
        return res.reply(message.server_error(), err.toString());
    }
};

module.exports = controller;
