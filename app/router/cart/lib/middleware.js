const { User } = require('../../../model');

const middleware = {};

middleware.verifyToken = async (req, res, next) => {
    try {
        const decoded = _.verifyToken(req.headers.authorization);

        req.userProfile = await User.findOne({ _id: decoded._id, sToken: req.headers.authorization });
        if (!req.userProfile) return res.reply(message.unauthorized('User'));

        next();
    } catch (err) {
        return res.reply(message.server_error());
    }
};

module.exports = middleware;
