const { Product, Category, Cart } = require('../../../model');

const controller = {}

controller.view = async (req, res) => {
    try {
        const query = [{
            $lookup: {
                from: "products",
                localField: "iProduct",
                foreignField: '_id',
                as: 'oProduct',
            }
        },
        {
            $unwind: '$oProduct',
        },
        {
            $addFields: {
                _id: { $toString: '$_id' },
                sTitle: '$oProduct.sTitle',
                sDescription: '$oProduct.sDescription',
                nPrice: '$oProduct.nPrice',
            },
        },
        {
            $project: {
                sTitle: 1,
                nPrice: 1,
                sDescription: 1,
                createdAt: 1,
            },
        }]

        const items = await Cart.aggregate(query);
        return res.reply(message.success(), items);
    } catch (error) {
        console.log(error);
        return res.reply(message.server_error(), error.toString());
    }
};

controller.add = async (req, res) => {
    try {
        const { iProductId } = _.pick(req.params, ['iProductId']);
        if (!iProductId) return res.reply(message.not_found('ID'));

        const newItem = new Cart({ iProduct: iProductId });
        await newItem.save();

        return res.reply(message.successfully("Added"));
    } catch (error) {
        console.log(error);
        return res.reply(message.server_error(), error.toString());
    }
}

controller.delete = async (req, res) => {
    try {
        const { iProductId } = _.pick(req.params, ['iProductId']);
        if (!iProductId) return res.reply(message.not_found('ID'));

        const item = await Cart.deleteOne({ iProduct: iProductId });
        return res.reply(message.successfully('Deleted'));
    } catch (error) {
        console.log(error);
        return res.reply(message.server_error(), error.toString());
    }
}

module.exports = controller;