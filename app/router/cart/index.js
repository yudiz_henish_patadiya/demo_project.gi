const router = require('express').Router();

const controller = require('./lib/controller');
const middleware = require('./lib/middleware');

router.get('/view', middleware.verifyToken, controller.view);
router.get('/add/:iProductId', middleware.verifyToken, controller.add);
router.get('/delete/:iProductId', middleware.verifyToken, controller.delete);

module.exports = router;
