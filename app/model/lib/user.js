const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
    {
        sFirstname: { type: String, required: true },
        sLastname: { type: String, required: true },
        sEmail: { type: String, required: true },
        nContactNo: { type: Number, required: true },
        sPassword: { type: String, required: true },
        sProfileUrl: { type: String },
        sToken: { type: String },
        eStatus: {
            type: String,
            enum: ['y', 'n', 'd'], // y: Active, n: InActive, d: Deleted
            default: 'y',
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model('User', userSchema);
