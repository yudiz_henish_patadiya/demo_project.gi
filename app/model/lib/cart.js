const mongoose = require('mongoose');

const cartSchema = mongoose.Schema(
    {
        iProduct: {
            type: mongoose.Schema.ObjectId,
            ref: 'Product'
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model('Cart', cartSchema);
