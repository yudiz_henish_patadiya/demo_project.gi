const mongoose = require('mongoose');

const categorySchema = mongoose.Schema(
    {
        sName: { type: String, required: true },
        eStatus: {
            type: String,
            enum: ['y', 'n', 'd'], // y: Active, n: InActive, d: Deleted
            default: 'y',
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model('Category', categorySchema);
