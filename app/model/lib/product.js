const mongoose = require('mongoose');

const productSchema = mongoose.Schema(
    {
        sTitle: { type: String, required: true },
        nPrice: { type: Number, required: true },
        sDescription: { type: String, required: true },
        iCategory: {
            type: mongoose.Schema.ObjectId,
            ref: 'Category',
        },
        eStatus: {
            type: String,
            enum: ['y', 'n', 'd'], // y: Active, n: InActive, d: Deleted
            default: 'y',
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model('Product', productSchema);
