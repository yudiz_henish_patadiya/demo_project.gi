const User = require('./lib/user');
const Product = require('./lib/product');
const Category = require('./lib/category');
const Cart = require('./lib/cart')

module.exports = {
    User,
    Product,
    Category,
    Cart
};
