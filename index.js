require('dotenv').config();
require('./globals');

const { database } = require('./app/utils');
const router = require('./app/router');

router.initialize();
database.initialize();
