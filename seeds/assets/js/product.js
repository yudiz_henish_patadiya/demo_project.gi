const box = document.getElementById('boxDiv');

(function view() {
    $.ajax({
        type: "GET",
        url: "/api/v1/product/view",
        headers: {
            'Authorization': token
        },
        contentType: 'application/json',
        success: async function (result, status, xhr) {
            // console.log(result);
            for (let i = 0; i < result.data.length; i++) {
                box.innerHTML += `<div class="col-xl-6 col-sm-6">
                <div class="card overflow-hidden">
                    <div class="card-header border-0">
                        <div class="d-flex">
                            <div class="invoices">
                                <h4>${result.data[i].sTitle}</h4>
                                <h3>Price: ${result.data[i].nPrice}</h3>
                                <h3>${result.data[i].sDescription}</h3>
                                <h3>Category: ${result.data[i].sCategoryName}</h3>
                                <button type="button" onclick="addtocart(${result.data[i]._id})">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`
            }
            toastr["success"](xhr.responseJSON.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
            toastr["error"]("Unauthorized, Please login !");
            setTimeout(() => {
                window.location.href = "/login";
            }, 1000)
        }
    });
})();

function addtocart(iProductId) {
    $.ajax({
        type: "GET",
        url: `/api/v1/cart/add/${iProductId}`,
        headers: {
            'Authorization': token
        },
        contentType: 'application/json',
        success: async function (result, status, xhr) {
            toastr["success"](xhr.responseJSON.message);
            // console.log(result);
            setTimeout(() => {
                window.location.href = "/cart";
            }, 1000)
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
            toastr["error"]("Unauthorized, Please login !");
            setTimeout(() => {
                window.location.href = "/login";
            }, 1000)
        }
    });
}