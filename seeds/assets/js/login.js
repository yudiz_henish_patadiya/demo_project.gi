// Function for email & password validation
function validateEmail(email) {
    var reg =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(String(email).toLowerCase());
}

let token = window.localStorage.getItem('Authorization');

// login
function signIn() {
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    if (validateEmail(email)) {
        const data = { sEmail: email, sPassword: password }
        // console.log(data);

        $.ajax({
            type: "POST",
            url: "/api/v1/user/login",
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: async function (result, status, xhr) {
                await window.localStorage.setItem('Authorization', result.data.sToken);
                toastr["success"](xhr.responseJSON.message);
                // console.log(result);
                setTimeout(() => {
                    window.location.href = "/";
                }, 1000)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                console.log(status);
                console.log(error);
                toastr["error"](xhr.responseJSON.message);
            }
        });
    }
    else {
        document.getElementById("errMsg").innerHTML = 'Please, Provide valid details !'
    }
}

// logout
function logout() {
    $.ajax({
        type: "POST",
        url: "/api/v1/user/logout",
        headers: {
            'Authorization': token
        },
        contentType: 'application/json',
        success: async function (result, status, xhr) {
            toastr["success"](xhr.responseJSON.message);
            // console.log(result);
            token = window.localStorage.getItem('Authorization');
            setTimeout(() => {
                window.location.href = "/login";
            }, 1000)
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
            toastr["error"]("Unauthorized, Please login !");
            setTimeout(() => {
                window.location.href = "/login";
            }, 1000)
        }
    });
}

// login
function register() {
    const email = document.getElementById('email').value;
    const firstname = document.getElementById('firstname').value;
    const lastname = document.getElementById('lastname').value;
    const contactno = document.getElementById('contactno').value;
    const password = document.getElementById('password').value;

    if (validateEmail(email)) {
        const data = { sEmail: email, sPassword: password, sFirstname: firstname, sLastname: lastname, nContactNo: contactno }
        // console.log(data);

        $.ajax({
            type: "POST",
            url: "/api/v1/user/register",
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: async function (result, status, xhr) {
                toastr["success"](xhr.responseJSON.message);
                // console.log(result);
                setTimeout(() => {
                    window.location.href = "/login";
                }, 1000)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                console.log(status);
                console.log(error);
                toastr["error"](xhr.responseJSON.message);
            }
        });
    }
    else {
        document.getElementById("errMsg").innerHTML = 'Please, Provide valid details !'
    }
}