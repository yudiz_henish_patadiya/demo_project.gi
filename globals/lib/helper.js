/* eslint-disable no-console */
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const _ = {};

const config = {
    JWT_SECRET: process.env.TOKEN_KEY,
};

_.pick = function (obj, array) {
    const clonedObj = this.clone(obj);
    return array.reduce((acc, elem) => {
        if (elem in clonedObj) acc[elem] = clonedObj[elem];
        return acc;
    }, {});
};

_.clone = function (data = {}) {
    const originalData = data.toObject ? data.toObject() : data; // for mongodb result operations
    const eType = originalData ? originalData.constructor : 'normal';
    if (eType === Object) return { ...originalData };
    if (eType === Array) return [...originalData];
    return data;
    // return JSON.parse(JSON.stringify(data));
};

_.encryptPassword = function (password) {
    return crypto
        .createHmac('sha256', config.JWT_SECRET)
        .update(password)
        .digest('hex');
};

_.encodeToken = function (body, expTime) {
    try {
        return expTime ? jwt.sign(this.clone(body), config.JWT_SECRET, expTime) : jwt.sign(this.clone(body), config.JWT_SECRET);
    } catch (error) {
        return undefined;
    }
};

_.decodeToken = function (token) {
    try {
        return jwt.decode(token, config.JWT_SECRET);
    } catch (error) {
        return undefined;
    }
};

_.verifyToken = function (token) {
    try {
        return jwt.verify(token, config.JWT_SECRET, function (err, decoded) {
            return err ? err.message : decoded; // return true if token expired
        });
    } catch (error) {
        return error ? error.message : error;
    }
};

_.reply = ({ code, message }, data = {}, header = undefined) => ({
    statusCode: code,
    headers: {
        ...header,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization',
        'Access-Control-Expose-Headers': 'Authorization',
    },
    body: JSON.stringify({ message, data }),
});

module.exports = _;
